## 1. Array and String
### Hash Table
* Map key to value 
* Implementation 
  * Compute the key’s hash code (int or long)
    * Two different key may have the same hash code 
  * Map hash code to an index in array `hash(key) % array.length`
    * Two different hash codes could map same index.
  * At index LinkedList of keys and values 
    * Store keys and values at the index.

### ArrayList and Resizable Array
* Array-like data structure offers dynamic resizing 
* Resize itself and provide o(1) access 
* When an array is full, double a size 
  * Each double-takes o(n)
* Why amortized insertion runtime o(1)
  * When we increase the array to K element 
  * The previous array is k/2 size so k/2 element 
  * So $n/2 + n/4 + n/8 + …. + 2 +1$.
  * Take $N$ insertion 

### StringBuilder

 
* What would be the running time of the list of strings concatenate be? 
  * Each concatenation, a new copy of the string is created and two string and copied over $O(xn^2)$
* StringBuilder avoids the problem. StringBuilder simply creates a resizable array of all the strings

### Questions 
* 1.1. isUnique: 
  we create an array that holds each weight of the char in the string. if we found that it occurs more than once it returns false 
  ```java 
  public boolean isUnique(String str){
      int[] unique = new int[128];
      for(char c : str.toCharArray()){
          int index = ((int) c) - 'A';
          if(index != 0) return false;
          unique[index]++;
      }

    return true;
  }
  ```
* 1.2 p
  
  ```java
  public boolean permutation(String str){
    int[] arr = new int[27];
    for(char c : str.toCharArray()){

    }
  }

  ```