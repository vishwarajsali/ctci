# Trees and Graphs 

## Types of Tree

- each tree has a root node 
- the root node has zero or more child nodes 
- each child node has zero or more child nodes and so on.


```java
class Nodde{
    public String name;
    public Node[] children;
}
```

```java
class Tree{
    public Node root;
}
```

### Depth first search

visit each brach before going to its neighbor

```java
void search (Node root){
    if(root == null) return;
    visit(root);
    root.visited = true;
    for each(Node n in root.adjacent){
        if(n.visited == false){
            search(n);
        }
    }
}
```

