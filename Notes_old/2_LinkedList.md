# 2. Linked List

represent sequence of Nodes.

- singly - each node point to next node
- Doubly each node point to both next and previous node

if you want to find a kth element you have to iterate through the k elements.

## Creating a linked list

```java
class Node{
    Node next = null;
    int data;
    public Node(int d){
        data = d;
    }

    void apendToTail(int d){
        Node end = new Node(d);
        Node n = this;
        while(n.next != null){
            n = n.next
        }
        n.next = end;
    }
}
```

## Deleting Node from singly linkedlist
we find the previous node `prev` and set `prev.next` to `n.next`

if its doubly - we also have to update `n.next` to set `n.next.prev` equal to `n.prev`
in this we have to check null pointer or update head or tail if necessary.

```java
Node deleteNode(Node head, int d){
    Node n = head;

    if(n.data == d) return head.next;

    while(n.next != null){
        if(n.next.data == d){
            n.next = n.next.next;
            return head;
        }
        n = n.next;
    }
    return head;
}
```

## The "Runner" Technique

runner -> second pointer
itrate through linkedlist w/ two pointer simultaneously, w/ one ahead of others

the "fast" node might be ahead by fixed amount or it might be hopping multiple node for each one node that the "slow" node iterate through.

for ex. a1-> a2 -> ....-> an -> b1 -> b2 -> ... -> bn
output a1 -> b1 -> a2 -> b2 ->.... -> an -> bn
p1(faster) move every two elements for evary one move the p2 when p1 at the end of the linkedlist and p2 will be at the mid
