
def product(a, b):
    prod = 0
    for i in range (b):
        prod += a
    return prod


print(product(2,3))

def SwapMinMax(int[] array):
    minIndex = 0
    for i in range(0, len(array)):
        if array[i] < array[minIndex]:
            minIndex = i
    maxIndex = 0
    for i in range(0, len(array)):
        if array[i] > array[maxIndex]:
            maxIndex = i
    
    temp = array[minIndex]
    array[minIndex] = array[maxIndex]
    array[maxIndex] = temp