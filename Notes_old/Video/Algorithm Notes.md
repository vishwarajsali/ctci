# Algorithm

## Graph Search, DFS and BFS

[video](https://www.youtube.com/watch?v=zaBhtODEL0w&list=PLX6IKgS15Ue02WDPRCmYKuZicQHit9kFt&index=6)

* a graph is a collection of nodes
  * where each node point to other nodes 
  * one way street or two way streets

1. Depth First Search
    tipically we uses the recursive algorith to solved the graph problems.
    Goes deep (in children) before going head(to neighbors)
    recursive algorithm

2. Breadth First Search
    - Goes broad(to neighbors) before going deep
    - level by level
    - iterative, using queue
    - need flag or someway of reventing infinite loop
    - rather than recursive it goes to queue 
  
    ```java
    public class Graph{
        private HashMap<Intger, Node> nodeLookup = new HashMap<Integer, Node>();

        public static class Node{
            private int id;
            LinkedList<Node> adjacent = new LinkedList<Node>();
            private Node(int id){
                this.id = id;
            }
        }

        private Node getNode(int id){

        }
        public void addEdge(int source, int destination){
            Node s = getNode(source);
            Node d = getNode(destination);
            s.adjacent.add(d);
        }

        public boolean hasPathDFS(int source, int destination){
            Node s = getNode(source);
            Node d = getNode(destination);
            HashSet<Integer> visited = new HashSet<Integer>();
            return hasPathDFS(s, d, visited);
        }

        private boolean hasPathDFS(Node source, Node destination, HashSet<Integer> visited){
            if(visited.contains(source.id)) return false;
            visited.add(source.id);
            if(source == destination) return true;

            for(Node child: source.adjecent){
                if(hasPathDFS(child, destination. visited)) return true;
            }

            return false;
        }

        public boolean hasPathBFS(int source, int destination){
            return hasPathBFS(getNode(source), getNode(destination));
        }
        private boolean hasPathBFS(Node source, Node destination){
            LinkedList<Node> nextToVisit = new LinkedList<Node>();
            HashSet<Integer> visited = new HashSet<Integer>();
            nextToVisit.add(source);
            while(!nextToVisit.isEmpty()){
                Node node = nextToVisit.remove();
                if(node == destination) return true;
                if(visited.contaons(node.id)) continue;

                visited.add(node.id);

                for(Node child: node.adjacent){
                    nextToVisit.add(child);
                }
             }

            return false;
        }
    }
    ```


## recurssion 
[Video](https://www.youtube.com/watch?v=KEEKn7Me-ms&list=PLX6IKgS15Ue02WDPRCmYKuZicQHit9kFt&index=7)

```java
int countTex(Directory dir){
    if(dir == null) return 0;
    int count = 0;

    for(File file : dir){
        if(file.endsWith(".txt")){
            count +=1;
        }
    }

    for (Directory subdir : dir){
        count += countTxt(subdir);
    }

    return count;
}
```

fibonnaci 

```java
int fib(int n){
    if(n<= 0) return 0;
    else if(n== 1) return 1;
    else return fib(n-1)+ fib(n-2);
}
```
anything recursive can be implemented interatively.

## Memorixation and Dynamic Programming

```java
int fib(int n , int[] mem){
    if(n<= 0) return 0;
    else if (n==1) return 1;
    else if (mem[n]){
        mem[n] = fib(n-1) + fin(n-2);
    } 
    return mem[n];
}
```

## Bit manipulation

* binary(positive)
* addition
* binary(negative)
* shifting
  * Logical vs Arithmetic(right shift)
* masks


## Binary Search 

log~2~(n)

### recursive
```java
public static boolean BinarySearchRecurive(int[] array, int x, int left, int right){
if(left > right) return false;
int mid = left + ((right - left)/ 2);

if(array[mid] == x) return true;
else if (x< array[mid]) return binarySearchRecursive(array, x,left, mid-1);
else return binarySearchRecursive(array, x, mid+1, right);

}

public static boolean binarySearchRecursive(int[] array, int x){
    return binarySearchRecusive(array, x, 0, array.length-1);
}
```

### Iterative

```java
public static boolean BinarySearchIterative(int[] array, int x){
    int left = 0, rigth = array.length-1;
    while(left <= right){
        int mid = left + ((right - left)/ 2);
        if(array[mid] == x) return true;
        else if (x< array[mid]) right = mid -1;
        else left = mid +1;
    }
    return false;
}
```

## Hash table

key -> value lookup

$Alice => new\ Person(id: 17, address : "17 hd dr.")$


```java
class hashtable{
    LinkedLis[] data
    boolean put(String key, person value){
        int hashcode = gethashCode(key);
        int index = Convert.ToIndex(hashcode);
        LinkedList list = data[index];
        list.insert(key, value);
    }
}
```

## Heaps 

[Video](https://www.youtube.com/watch?v=t0Cq6tVNRBA&list=PLX6IKgS15Ue02WDPRCmYKuZicQHit9kFt&index=12)

Two type 
- min 
- max

min -> always root node is the min od the tree 

```java
class HeapNode{
    HeapNode left, right;
}
```

```java
public class MinIntHeap{
    private int capacity = 10;
    private int size = 0;

    int[] items = new int[capacity];
    

    private int getLeftChildIndex(int parentIndex) {return 2 * parrentInde +1;}
    private int getrightChildIndex(int parentIndex) {return 2 * parrentInde +2;}
    private int getParentIndex(int childIndex) {return (childIndex-1)/2;}

    private boolean hasLeftChild(int index) { return getLeftChildIndex(index) <size;}
    private boolean hasRightChild(int index) { return getRightChildIndex(index) <size;}
    private boolean hasParent(int index) { return getParentIndex(index)>= 0;}


    private int leftChild (int index) {return items[getLeftChildIndex(index)];}
    private int rightChild (int index) {return items[getRightChildIndex(index)];}
    private int parent (int index) {return items[getParentChildIndex(index)];}

    private void swap(int indexOne, int indexTwo){
        int temp = items[indexOne];
        items[indexOne] = items[indexTwo];
        items[indexTwo] = temp;
    }

    private void ensureExtraCapacity(){
        if(size == capacity )
    }

}
```
