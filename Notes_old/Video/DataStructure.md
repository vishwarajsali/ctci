# DataStructure 

## Balanced parentheses in Expression

is String Balanced ?   
$ \{\}    []  () $
Squggles  brackets parens 

Ex.
{()[{({})[]()}]}([])

```java
 private static char[][] TOKEN = {{'{', '}'}, {'[', ']'}, {'(', ')'}};

    private static boolean isOpend(char c){
        for(char[] array: TOKEN){
            if(array[0] == c) return true;
        }
        return false;
    }

    private static boolean isMatch(char openExpre, char closeExpre){
        for(char[] array : TOKEN){
            if(array[0]== openExpre) return array[1] == closeExpre;
        }
        return false;
    }

    public static boolean isBalanced(String expression){

        Stack<Character> stack = new Stack<>();

        for(char c : expression.toCharArray()){
            if(isOpend(c)) stack.push(c);
            else {
                if(stack.isEmpty() || !isMatch(stack.pop(), c)) return false;
            }
        }

        return stack.isEmpty();
    }
```

## Queue with two stack

stack stores the newest item on the top so oldest item at the bottom
FILO

we store all item in newest stack from newest to oldest and then tranfer it into oldestStack from new to old.

```java
private  Stack<T> stackNewestOnTop = new Stack<>();
private  Stack<T> stackOldestOnTop = new Stack<>();

public void enqueue(T value){
    stackNewestOnTop.push(value);
}

public T peek(){
    shiftStack();
    return stackOldestOnTop.peek();
}

private void shiftStack(){
    if(stackOldestOnTop.isEmpty()){
        while (!stackNewestOnTop.isEmpty()) {
            stackOldestOnTop.push(stackNewestOnTop.pop());
        }
    }
}

public T dequeue(){
    shiftStack();
    return stackOldestOnTop.pop();
}
````

## Stack and Queue

- Liner Data Structure
- Flexible in size 

stack - Last in first out structure 
queue - first in first out structure

Queue 
```java
private static class Node{
    private int data;
    private Node next;

    private Node(int data){
        this.data = data;
    }
}

private Node head, tail;

public boolean isEmpty(){
    return head == null;
}

public int peek(){
    return head.data;
}

public void add(int data){
    Node node = new Node(data);
    if(tail!= null) tail.next = node;
    tail = node;
    if(head == null) head = node;

}

public int remove(){
    int data = head.data;
    head = head.next;
    if(head == null) tail = null;
    return data;
}
```

Stack

```java
private static class Node{
    private int data;
    private Node next;
    private Node(int data){
        this.data = data;
    }
}

private Node top;

public boolean isEmpty(){ return top == null;}

public int peek(){ return top.data; }

public void push(int data){
    Node node = new Node(data);
    node.next = top;
    top = node;
}

public int pop(){
    int data = top.data;
    top = top.next;
    return data;
}
```