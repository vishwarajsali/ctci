# Cracking the Coding interview

## __7 Steps to Solve Algorithm Problems__
[Video](https://www.youtube.com/watch?v=GKgAVjJxh9w&list=PLX6IKgS15Ue02WDPRCmYKuZicQHit9kFt&index=1)

* Listen - typically every detail is needed to solve problem - so if you solve a problem without using a detail, you might need that to solve it optimally.  
Q. Given two arrays __(sorted + distinct)__ find # eleents in common.

* Example -
    Q. Given two arrays __(sorted + distinct)__ find # eleents in common.

``` java
//too small with special case
A: 1,5,15,20
B: 2,5,13,30

//
A: 1,5,15,20,30,37
B: 2,5,13,30,32,35,37,42
```

* Brute Force  
better to have a brute force than nothing at all  
because :

    * checks that you understand the problem
    * shows you're at least good enogh to get that
    * Its a good place to optimize from

    Dont code it

    1. state brute force
    1. state runtime
    1. Optimize

* Optimize

* walk through your algorithm
    know exactly what you are going to do before coding

    * what variables and data structure?
    * How, when + why do they change?
    * what is the structue of your code?

* Code

    * WhiteBoard
        * write straight
        * user space wisely
            * erase what you dont need
            * ok to user arrows
            * write in top left corner
    * whiteBoard or Computer
        * coding style matters
            * consistent braces
            * consistent variable naming
            * consistent spaces
            * Descriptive variable
        * Modularize (before not after!)

* Test  Not good test cases

    * Analysis
        * Think about each line
        * double check things that look weird / riskky
    * use test cases
        * small test case first
            * faster to run
            * you will probably be more thorough
        * edge case
        * big test cases

    __Remember:__  
    * Think as you test  
        * dont be a bot!
    * Test your code not your algorithm 
    * think before you fix bugs. Dont panic

## __3 Algorithm Strategies__

[Video](https://www.youtube.com/watch?v=84UYVCluClQ&index=2&list=PLX6IKgS15Ue02WDPRCmYKuZicQHit9kFt)  

1. BUD (bottlenecks Unnecessary work Duplicated work)
1. Space/ Time Trade offs
1. DIY (do it Yourself)

__Bottleneacks__

```
A: 1,5,12,3,-15,52
B: 3,1,6,5,57,13,17
```

brute Force: O(A*B) - not $O(N^2)$

__Unnecessary Work__

$a^3 + b^3=c^3+d^3$  
$0<a,b,c,d<1000$

```sudo
for a 1 -> 1000
    for b 1 -> 1000
        for c 1 -> 1000
            for d 1 -> 1000
                if(a * a + b* b == c*c )
```

is there a faster way to find the  
solve for d : $d=\sqrt[3](a^3 + b^3 - c^3)$

* __Space / Time TradeOffs__  
Always have Hashtable at the top of your mind

* __DIY__
  
  find all permutations of s within b
  brute force:
  * create list od all permutations of s
  * find all within b
  
  s = xacxzaa
  b = fxaazxacaaxzoecazxaxaz
  
  ```java
  public void StringPermutation(String s, String b) {
        int i=0, j = 0, count =0;
        boolean isP = false;
        while (i < b.length()-s.length()) {
            j = i;
            while (j <s.length()+i+1) {
                isP = false;
                int k=0;
                while (k<s.length()) {
                    if(b.charAt(j) == s.charAt(k)) isP = true;
                    k++;
                }
                if(!isP) break;
                j++;
            }
            if(isP) System.out.println(b.substring(i, j));
            i++;
        }
    }
  ```
  
## How to Approach Behavioral Questions
[Video](https://www.youtube.com/watch?v=tZxNNKqxXnw&index=3&list=PLX6IKgS15Ue02WDPRCmYKuZicQHit9kFt)

Quick walk-through your resume

```mermaid
    graph TD;
        A[headLine] --> B[Beginning of career]
        B --> C[Go through FORWARD chronologically]
```

  1.  Quick show of success 
  2.  key stories 
      1.  just hint. let the interviewer ask for details 
  3.  Hobbies 
      1.  technical 
      2.  non- technical (try to make them relevant)
  4.  prepare to discuss at least 2-3 projects in details
      1.  technical stuff
      2.  soft skills



## Big O Notation
[Video](https://www.youtube.com/watch?v=v4cd1O4zkGw&list=PLX6IKgS15Ue02WDPRCmYKuZicQHit9kFt&index=5)
    data $\propto$ time
__Big O__: How time scales with respect to some timput variables

$O(N)$ : when N is the size of the array

```psedo
boolean contains(array, x){
    foreach element in array{
        if element == x{
            return true
        }
    }
}
```

$O(N^2)$

```psedo
void printPair(array){
    foreach x in array{
        foreach y in array{
            print x, y
        }
    }
}
```

1. Different steps get added 
2. Drop contants
3. Differnt inout $\Rightarrow$ differnt variables

    ```psedo
    int intersectionSize (array A, array B):
        int count = 0
        for a in arrayA:
            for b in arrayB:
                if a == b:
                    count += 1
        return count
    ```

    $O(a * b)$
1. Drop non-dominate terms

    ```psedo
        funtion whyWouldIDoThis (array):
            max = null
            foreach a in array:
                max = Max(a, max)
            print max

            foreach a in array:
                foreach b in array:
                    print a, b
    ```

    $O(N) + O(N^2) = O(N^2)$
    $O(N^2) \lesssim O(N + N^2) \lesssim O(N^2 + N^2)$
    $O(N + N^2) \Rightarrow O(N^2)$

