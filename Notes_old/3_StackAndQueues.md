# 3. Stack and Queues

## Implementing a Stack

- stack of data
- LIFO - last in frist out

- oprations 
  - pop() - Remove the top itemfrom the stack
  - push(item) - Add an item to the top of the stack 
  - peek() - return the top of the stack
  - isEmpty() - return true id and only if the stack is empty.

unlike an array stack does not offer constant time access to the ith item. however, offer constant time to adds and removes 


## Implementing a Queue 
FIFO - First in first out
items are removed from the data structure in the same order they are added.

opration 
- add(item) - add to the end 
- remove() - remove first
- peek() return top of queue
- isEmpty() if queue is empty
  
  