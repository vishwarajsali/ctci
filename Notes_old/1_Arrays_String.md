

# Arrays And Strings

## Hash Tables

- Two different keys could have same hashcode 
- Two different hash code could map to the same key
  
if Number of collision is very high the worst case runtime is $O(N)$ 
However, good implementation that keeps collisions to minimum, lookup time is O(1).


0 0 1 1 1 2 2 3 3 4


## Arraylist and Resizable arrays 
its automaticatlly resizable
it resize itself as needed while still providing O(1) 

```java
ArrayList<String> merge (String[] word, String[] more){
    ArrayList<String> sent = new ArrayList<>();
    for(String w : word) sent.add(w);
    for(String w : more) sent.add(w);
    return sent;
}
```

## StringBuilder
```java
String joinWords(String[] word){
    String sent = "";
    for(String s : word){
        sent += s;
    }
    return sent;
}
```
on each concatenation, a new copy of the string is created and string is copy over char by char.        
we can avoid this by using StringBuilder
StringBuilder - Creates a resizable array of all the strings and copying them back to string when it necessary.

```java
String joinWords(String[] word){
    StringBuilder sent = new StringBuilder();
    for(String w : word){
        sent.append(w);
    }
    return sent.toString();
}
```
