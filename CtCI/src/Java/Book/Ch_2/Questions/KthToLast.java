package Java.Book.Ch_2.Questions;

import Java.Book.CtCILib.LinkedList.SinglyList.Node;

public class KthToLast {
    public static void main(String[] args) {
        Node root = new Node(1);
        root.appendToTail(2);
        root.appendToTail(3);
        root.appendToTail(4);
        root.appendToTail(5);
        root.appendToTail(6);

        System.out.println(root);

        KthToLast s = new KthToLast();
        System.out.println(s.returnKthToLast(root, 4));

    }

    // Time O(N) 
    private Node returnKthToLast(Node root, int k) {

        while (k > 1) {
            root = root.next;
            k--;
        }

        return root;
    }
}