package Java.Book.Ch_2.Questions;

import java.util.HashSet;
import java.util.Set;

import Java.Book.CtCILib.LinkedList.SinglyList.Node;

/**
 * RemoveDups
 */
public class RemoveDups {

  public static void main(String[] args) {
    Node root = new Node(1);
    root.appendToTail(2);
    root.appendToTail(3);
    root.appendToTail(4);
    root.appendToTail(1);
    root.appendToTail(2);

    System.out.println(root);

    RemoveDups r = new RemoveDups();

    System.out.println(r.removeDups1(root));
    System.out.println(r.removeDups(root));

  }

  // Time Comp - O(N^2);
  private Node removeDups(Node root) {
    Node ptr1 = root, ptr2;
    while (ptr1 != null && ptr1.next != null) {
      ptr2 = ptr1;
      while (ptr2.next != null) {

        if (ptr1.val == ptr2.next.val) {
          ptr2.next = ptr2.next.next;
        } else {
          ptr2 = ptr2.next;
        }
      }
      ptr1 = ptr1.next;
    }
    return root;
  }

  // Time Comp - O(N) 
  //Space -> O(N)
  private Node removeDups1(Node root) {
     Set<Integer> set = new HashSet<>();
     
     Node p1 = root;
     while(p1.next != null){
       if(set.contains(p1.next.val)){
         p1.next = p1.next.next;
         continue;
       }
       set.add(p1.val);
      p1 = p1.next;
     }

     return root;
  }

}