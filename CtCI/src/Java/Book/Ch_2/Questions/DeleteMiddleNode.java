package Java.Book.Ch_2.Questions;

import Java.Book.CtCILib.LinkedList.SinglyList.Node;

public class DeleteMiddleNode {

    public static void main(String[] args) {
        Node root = new Node(1);
        root.appendToTail(2);
        root.appendToTail(3);
        root.appendToTail(4);
        root.appendToTail(5);
        root.appendToTail(6);

        System.out.println(root);

        DeleteMiddleNode s = new DeleteMiddleNode();
        System.out.println(s.deleteMiddleNode(root));

    }

    private Node deleteMiddleNode(Node root) {
        Node slow = root, fast = root, prev = null;
        while (fast != null && fast.next != null) {
            fast = fast.next.next;
            prev = slow;
            slow = slow.next;
        }
        prev.next = slow.next;

        return root;
    }
}