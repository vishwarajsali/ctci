package Java.Book.Ch_1;

import java.util.Arrays;

 

public class Ch_1_Question {

    public static void main(String[] args) {
        Ch_1_Question c = new Ch_1_Question();

        //System.out.println(c.isUnique("abc"));
        //System.out.println(c.permutation("tea", "ate"));
        //System.out.println(c.urlify("Mr John Smith      ", 13));
        //System.out.println(c.oneAway("pale", "bake"));
        //System.out.println(c.strCompression("aabcccccaaa"));
        //int[][] matrix = {{1,2,3}, {4,5,6},{7,8,9}};
        //System.out.println(Arrays.deepToString(c.rotateMatrix(matrix)));
        //int[][] matrix = {{1,2,3 , 4}, {0,5,6, 5},{7,8,9, 5}};
        //System.out.println(Arrays.deepToString(c.zeroMatrix(matrix)));
    }

    /**
     * 1.1 
     * Implement an algorithm to determine if a string has all unique characters.
     * What if you cannot use additional data structures?
     * @param str
     * @return
     */
    public boolean isUnique(String str){
        int[] unique = new int[128];
        for(char c : str.toCharArray()){
            int index = ((int) c) - (int)'A';
            if(unique[index] != 0) return false;
            unique[index]++;
        }
  
      return true;
    }

    /**
     *  Given two strings, write a method to decide if one is the permutation of other.
     * @param s1
     * @param s2
     * @return
     */

    public boolean permutation(String s1, String s2){
        if(s1.length() != s2.length()) return false;
        int[] arr = new int[128];
        for(char c : s1.toCharArray()){
            int index = ((int) c) - (int)'A';
            arr[index]++;
        }

        for(char c : s1.toCharArray()){
            int index = ((int) c) - (int)'A';
            arr[index]--;
            if(arr[index] < 0) return false;
        }

        for(Integer i: arr){
            if(i != 0) return false;
        }

        return true;
    }

    public String urlify(String str, int size){
        String[] input = str.split("/+s");
        StringBuilder output = new StringBuilder();
        Arrays.toString(input);
        for(int i = 0; i< 13; i++){
            if(str.charAt(i) == ' '){
                output.append("%20");
            }else output.append(str.charAt(i));
        }

        return output.toString();
    }

    /**
     * 
     * @param str
     * @return
     */

    public boolean permutation(String str){
        int[] arr = new int[128];
        int countOdd = 0;
        for(char c : str.toCharArray()){
            int i = ((int) c) - (int)'A';
            arr[i]++;
            if(arr[i]% 2 == 1){
                countOdd++;
            }else countOdd--;
        }

        return countOdd <= 1;

      }


    /**
     * 
     * @param str1
     * @param str2
     * @return
     */

    private boolean oneAway(String str1, String str2){

        int[] arr = new int[128];

        for(char c : str1.toCharArray()){
            int i = ((int) c) - (int)'A';
            arr[i]++;
        }

        for(char c : str2.toCharArray()){
            int i = ((int) c) - (int)'A';
            arr[i]--;
        }

        int count = 0;
        for(int i : arr){
            if(i > 1 || i <= -1) count++;
            if(count > 1) return false;
        }

        return true;
    }

    /**
     * 
     * @param str
     * @return
     */

    private String strCompression(String str){
        StringBuilder sb = new StringBuilder();

        char prev = str.charAt(0);
        int count = 0;
        for(char c : str.toCharArray()){
            if(prev != c){
                sb.append(prev);
                sb.append(count);
                count = 1;
            }

            if(prev == c) count++;
            prev = c;
        }
        sb.append(prev);
        sb.append(count);

        return sb.toString();
    }


    /**
     * 
     * @param matrix
     * @return
     */
    private int[][] rotateMatrix(int[][] matrix){
        int n = matrix.length;
        for(int i = 0; i< n/2; i++){
            for(int j = i; j <  n - i -1; j++){
                int temp = matrix[i][j]; 
   
                matrix[i][j] = matrix[j][n - 1 - i]; 
   
                matrix[j][n - 1 - i] = matrix[n - 1 - i][n - 1 - j]; 
   
                matrix[n - 1 - i][n - 1 - j] = matrix[n - 1 - j][i]; 
   
                matrix[n - 1 - j][i] = temp; 
            }
        }

        return matrix;
    }


    /**
     * 
     * @param matrix
     * @return
     */
    private int[][] zeroMatrix(int[][] matrix){
        int row = -1, col = -1;
        for(int i = 0; i< matrix.length; i++){
            for(int j = 0; j< matrix[0].length; j++){
                if(matrix[i][j] == 0){
                    row = i;
                    col = j;
                }
            }
        }

        for(int i = 0; i< matrix.length; i++){
            for(int j = 0; j< matrix[0].length; j++){
                if(i == row || j == col){
                    matrix[i][j] = 0;
                }
            }
        }

        return matrix;
    }

    /**
     * 
     * @param s1
     * @param s2
     */
    public  void isSubString(String s1, String s2){
    
    }
}