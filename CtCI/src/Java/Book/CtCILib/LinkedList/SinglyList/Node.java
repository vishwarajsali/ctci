package Java.Book.CtCILib.LinkedList.SinglyList;

public class Node {
    public Node next;
    public int val;
    public Node(int val){
        this.val = val;
    }

    public Node( ){ 
    }

    public void appendToTail(int val){
        Node end = new Node(val);
        Node curr = this;
        while(curr.next != null){
            curr = curr.next;
        }
        curr.next = end;
    }

    public Node deleteNode(int val){
        Node curr = this;

        if(curr.val == val) return curr.next;

        while(curr.next != null){
            if(curr.next.val == val){
                curr.next = curr.next.next;
                return curr;
            }
            curr = curr.next;
        }

        return curr;
    }

    public void printSingly(){
        Node curr = this;
        while(curr != null){
            System.out.printf(" %d -> ",curr.val);
            curr = curr.next;
        }
        System.out.printf("null");

    }

    @Override
    public String toString() {
        Node curr = this; 
        while(curr != null){
            System.out.printf(" %d -> ",curr.val);
            curr = curr.next;
        }
        //System.out.printf("null");
        return null;
    }
    

}
 