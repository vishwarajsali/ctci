package Java.Book_old.Ch_4;

import Java.Book_old.CtCILib.HelpingMethond;
import Java.Book_old.CtCILib.TreeNode;

/**
 * Traversal
 */
public class Traversal {

    public static void main(String[] args) {
        
        int[] array = HelpingMethond.createAsLengthArray(10);
        TreeNode root = TreeNode.createMinimalBST(array);

        Traversal traversal = new Traversal();

        traversal.inOrder(root);
    }

    public void inOrder(TreeNode root){

        if(root == null) return;
        inOrder(root.left);

        System.out.printf("%d ", root.data);
        
        inOrder(root.right);
    }


}