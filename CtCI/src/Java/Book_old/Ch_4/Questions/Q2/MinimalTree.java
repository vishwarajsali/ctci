package Java.Book_old.Ch_4.Questions.Q2;

import Java.Book_old.CtCILib.TreeNode;
/**
 * MinimalTree
 */
public class MinimalTree {

    public static void main(String[] args) {
        int[] array = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

        TreeNode root = TreeNode.createMinimalBST(array);
        System.out.println("Root? " + root.data);
		

    }
}