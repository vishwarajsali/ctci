package Java.Book_old.Ch_4.Questions.Q6;

import Java.Book_old.CtCILib.HelpingMethond;
import Java.Book_old.CtCILib.TreeNode;

/**
 * Successor_1
 */
public class Successor_1 {
    public static void main(String[] args) {
        int[] array = HelpingMethond.createAsLengthArray(10);
        TreeNode root = TreeNode.createMinimalBST(array);

        Successor_1 successor_1 = new Successor_1();
        TreeNode result =  successor_1.getSuccessor(root, 8);
        System.out.println(result.data);
    }

    public TreeNode getSuccessor(TreeNode root, int data){
        TreeNode current = TreeNode.findNodeByData(root, data);
        if(current == null) return null;

        if(current.right != null){
            return TreeNode.findLeftMostNode(root.right);
        }else{
            TreeNode successor = null;
            TreeNode ancestor = root;

            while(ancestor != current){
                if(current.data < ancestor.data){
                    successor = ancestor;
                    ancestor = ancestor.left;
                }else{
                    ancestor = ancestor.left;
                }
            }
            return successor;
        }
    }
}