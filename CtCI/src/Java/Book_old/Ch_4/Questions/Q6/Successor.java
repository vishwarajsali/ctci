package Java.Book_old.Ch_4.Questions.Q6;

import Java.Book_old.CtCILib.TreeNode;

/**
 * Successor
 */
public class Successor {

    public static void main(String[] args) {
        int[] array = {1,2,3,4,5,6,7,8,9,10};
        TreeNode root = TreeNode.createMinimalBST(array);

        for(int i =0; i< array.length; i++){

        }
    }


    public TreeNode inOrderSucc(TreeNode root){
        if(root.parent == null || root.right != null){
            return leftMostChild(root.right);
        }
        else {
            TreeNode curr = root;
            TreeNode currParent = curr.parent;

            while(currParent != null  && currParent.left != curr){
                curr = currParent;
                currParent = currParent.parent;
            }

            return currParent;
        }
    }

    private TreeNode leftMostChild(TreeNode root){
        if(root == null) return null;

        while(root.left != null){
            root = root.left;

        }

        return root;
    }
}