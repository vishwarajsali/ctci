package Java.Book_old.Ch_4.Questions.Q7;

import java.util.ArrayList;
import java.util.Stack;

/**
 * BuildOrder
 */
public class BuildOrder {

    public Graph_Project buildGraph(String[] projects, String [][] dependencies){
        Graph_Project graph = new Graph_Project();
        for(String [] dependency: dependencies){
            String first = dependency[0];
            String second = dependency[1];

        }
        return  graph;
    }

    public boolean doDFS(Project project , Stack<Project> stack){
        if(project.getState()== Project.State.PARTIAL){
            return false;

        }

        if(project.getState() == Project.State.BLANK){
            project.setState(Project.State.PARTIAL);
            ArrayList<Project> children = project.getChildren();
            for(Project child: children){
                if(!doDFS(child, stack)){
                    return false;
                }
            }
            project.setState(Project.State.COMPLETE);
            stack.push(project);
        }
        return true;
    }

    public Stack<Project> orderProjects(ArrayList<Project> projects){
        Stack<Project> stack = new Stack<>();

        for(Project project : projects){
            if(project.getState() == Project.State.BLANK){
                if(!doDFS(project, stack)) return null;
            }
        }

        return  stack;
    }

    public String[] convertToStringList(Stack<Project> projects){
        String[] buildOrder = new String[projects.size()];
        for(int i = 0; i< buildOrder.length; i++){
            buildOrder[i] = projects.pop().getName();
        }

        return buildOrder;
    }

    public Stack<Project> findBuildOrder(String[] projects, String[][] dependencies){
        Graph_Project graph = buildGraph(projects, dependencies);
        return  orderProjects(graph.getProjects());

    }

    public String[] buildOrderWrapper(String[] projects, String[][] dependencies){
        Stack<Project> buildOrder = findBuildOrder(projects, dependencies);
        if(buildOrder == null) return  null;
        String[] buildOrderString = convertToStringList(buildOrder);
        return buildOrderString;
    }


    public static void main(String[] args) {
        String[] projects = {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j"};
        String[][] dependencies = {
                {"a", "b"},
                {"b", "c"},
                {"a", "c"},
                {"d", "e"},
                {"b", "d"},
                {"e", "f"},
                {"a", "f"},
                {"h", "i"},
                {"h", "j"},
                {"i", "j"},
                {"g", "j"}};
        BuildOrder bd = new BuildOrder();
        String[] buildOrder = bd.buildOrderWrapper(projects, dependencies);
        if (buildOrder == null) {
            System.out.println("Circular Dependency.");
        } else {
            for (String s : buildOrder) {
                System.out.println(s);
            }
        }
    }
    
}