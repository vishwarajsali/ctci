package Java.Book_old.Ch_4.Questions.Q7;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Project
 */
public class Project {

    public enum State {COMPLETE, PARTIAL, BLANK};
    private ArrayList<Project> children = new ArrayList<>();
    private HashMap<String, Project> map = new HashMap<>();
    private String name;
    private State state = State.BLANK;


    public Project(String name){
        this.name = name;
    }

    public ArrayList<Project> getChildren() {
        return children;
    }


    public String getName() {
        return name;
    }


    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public void addNeighbor(Project project){
        if(!map.containsKey(project.getName())){
            children.add(project);
            map.put(project.getName(), project);
        }
    }

    
}