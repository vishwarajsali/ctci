package Java.Book_old.Ch_4.Questions.Q7;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Graph_Project
 */
public class Graph_Project {

    private ArrayList<Project> projects = new ArrayList<>();
    private HashMap<String, Project> map = new HashMap<>();

    public Project getOrCreateNode(String name){
        if(!map.containsKey(name)){
            Project project = new Project(name);
            projects.add(project);
            map.put(name, project);
        }

        return map.get(name);
    }

    public void addEdge(String startName, String endName){
        Project start = getOrCreateNode(startName),
                end = getOrCreateNode(endName);
        start.addNeighbor(end);
    }

    public ArrayList<Project> getProjects(){
        return projects;
    }
}