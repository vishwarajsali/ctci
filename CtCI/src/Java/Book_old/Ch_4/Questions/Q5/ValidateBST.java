package Java.Book_old.Ch_4.Questions.Q5;

import Java.Book_old.CtCILib.TreeNode;

/**
 * ValidateBST
 */
public class ValidateBST {

    public static Integer lastPrinted = null;

    public static void main(String[] args) {

        ValidateBST vBST = new ValidateBST();
        int[] array = {Integer.MIN_VALUE, Integer.MAX_VALUE - 2, Integer.MAX_VALUE - 1, Integer.MAX_VALUE};
        TreeNode node = TreeNode.createMinimalBST(array);
        System.out.println(vBST.checkBST(node));
    }

    public boolean checkBST(TreeNode node){
        return checkBST(node, true);
    }

    private boolean checkBST(TreeNode node, boolean isLeft){
        if(node == null) return true;

        if(!(checkBST(node.left, true))) return false;

        if(lastPrinted != null){
            if(isLeft){
                if(node.data < lastPrinted) return false;
            } else{
                if(node.data <= lastPrinted) return false;
            }
        }
        lastPrinted = node.data;

        if(!(checkBST(node.right, false))){
            return false;
        }

        return true;
    }
}