package Java.Book_old.Ch_3.Questions;

import java.util.EmptyStackException;

/**
 * ThreeInOne
 */
public class ThreeInOne {

    private int noOfStack = 3;
    private int stackCapacity;
    private int[] values;
    private int[] sizes;

    public ThreeInOne(int stackSize) {
        stackCapacity = stackSize;
        values = new int[stackCapacity * noOfStack];
        sizes = new int[noOfStack];
    }

    public void push(int stackNum, int value) throws Exception{
        if (isFull(stackNum)) throw new Exception("Stack overflow");

        sizes[stackNum]++;
        values[indexOfTop(stackNum)] = value;
    }

    public int pop(int stackNum) {
        if (isEmpty(stackNum))
            throw new EmptyStackException();
        int topIndex = indexOfTop(stackNum);
        int value = values[topIndex];
        values[stackNum] = 0;
        sizes[stackNum]--;
        return value;
    }

    public int peek(int stackNum) {
        if (isEmpty(stackNum))
            throw new EmptyStackException();
        return values[indexOfTop(stackNum)];
    }

    public boolean isEmpty(int stackNum) {
        return sizes[stackNum] == 0;
    }

    private int indexOfTop(int stackNum) {
        int offset = stackNum * stackCapacity;
        int size = sizes[stackNum];
        return offset + size - 1;
    }

    public boolean isFull(int stackNum) {
        return sizes[stackNum] == stackCapacity;
    }
}