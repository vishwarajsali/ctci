package Java.Book_old.Ch_3.Questions.Q5;

import java.util.Stack;

/**
 * SortedStack
 */
public class SortedStack {

    public Stack<Integer> sort(Stack<Integer> input){
        Stack<Integer> temp = new Stack<>();

        while(!input.isEmpty()){
            int value = input.pop();

            while(!temp.isEmpty() && temp.peek() > value){
                input.push(temp.pop());
            }

            temp.push(value);
        }


        return temp;
    }
}