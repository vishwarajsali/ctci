package Java.Book_old.Ch_3.Questions.Q3;

import java.util.EmptyStackException;

/**
 * MyStack
 */
public class MyStack {

    private class MyStackNode{
        private MyStackNode below;
        private MyStackNode above;
        private int data;

        public MyStackNode(int data){
            this.data = data;
        }
    }


    private int capacity;
    private MyStackNode top, bottom;
    public int size;

    public MyStack(int capacity){
        size = 0;
        this.capacity = capacity;
    }

    public boolean isFull(){
        return capacity == size;
    }

    public void join(MyStackNode above, MyStackNode below){
        if(below != null) below.above = above;
        if(above != null) above.below = below;
    }

    public void push(int item){

        MyStackNode t = new MyStackNode(item);
        if(top != null) bottom = t;

        join(t, top);
        top = t;
    }

    public int pop(){
        if(top == null) throw new EmptyStackException();
        int item = top.data;
        top = top.below;
        return item;

    }

    public boolean isEmpty(){
        return top == null;
    }

    public int removeBottom(){
        int t = bottom.data;
        bottom = bottom.above;
        return t;

    }
}