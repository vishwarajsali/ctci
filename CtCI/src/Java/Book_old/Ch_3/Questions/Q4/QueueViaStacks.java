package Java.Book_old.Ch_3.Questions.Q4;

import java.util.Stack;

/**
 * QueueViaStacks
 */
public class QueueViaStacks<T> {
    
    Stack<T> stackNewest, stackOldest;

    public QueueViaStacks(){
        stackNewest = new Stack<T>();
        stackOldest = new Stack<T>();
    }

    public int size(){
        return stackNewest.size() + stackOldest.size();
    }

    public void add(T value){
        stackNewest.push(value);
    }

    private void shift(){
        if(stackOldest.isEmpty()){
            while(!stackNewest.isEmpty()){
                stackOldest.push(stackNewest.pop());
            }
        }
    }

    public T peek(){
        shift();
        return stackOldest.peek();
    }

    public T remove(){
        shift();
        return stackOldest.pop();
    }

    
}