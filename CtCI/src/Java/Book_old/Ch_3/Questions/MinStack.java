package Java.Book_old.Ch_3.Questions;
import java.util.EmptyStackException;

/**
 * MinStack
 */
public class MinStack {

    private class MinStackNode{

        private MinStackNode next;
        private int data;
        private int min;

        public MinStackNode(int data, int min){
            this.data = data;
            this.min = min;
        }
    }


    private MinStackNode top;

    public void push(int item){
        MinStackNode t = new MinStackNode(item, Math.min(item, top.min));
        t.next = top;
        top = t;
    }

    public int pop(){
        if(top == null) throw new EmptyStackException();
        int item = top.data;
        top = top.next;
        return item;
    }

    public int peek(){
        if(top == null) throw new EmptyStackException();
        return top.data;
    }

    public int getMin(){
        if(top == null) throw new EmptyStackException();
        return top.min;
    }

}