package Java.Book_old.Ch_5.Questions.Q1;

import Java.Book_old.CtCILib.HelpingMethond;

/**
 * Insertion
 */
public class Insertion {

    public static void main(String[] args) {
        
        int a = ~23423;
		System.out.println(HelpingMethond.toFullBinaryString(a));
		int b = 5;
		System.out.println(HelpingMethond.toFullBinaryString(b));		
		int c = insertion(a, b, 29, 31);
        System.out.println(HelpingMethond.toFullBinaryString(c));
    }

    public static int insertion(int n, int m,  int i, int j){

		if (i > j || i < 0 || j >= 32) {
			return 0;
		}
	
		int allOnes = ~0;
		
		int left = j < 31 ? (allOnes << (j + 1)) : 0; 	
	  	int right = ((1 << i) - 1); 
		int mask = left | right; 
		int n_cleared = n & mask;
		int m_shifted = m << i; 
		
		return n_cleared | m_shifted; 
    }

    
}