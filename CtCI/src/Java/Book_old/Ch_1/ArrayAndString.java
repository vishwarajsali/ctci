package Java.Book_old.Ch_1;


/**
 * ArrayAndString
 */
public class ArrayAndString {

  public static void main(String[] args) {
    // System.out.print(IsUnique("abc"));
    // System.out.println(CheckPermutation("geeksforgeeks", "frgeeksgeekso"));
    // System.out.println(URLify("Mr John Smith ", 13));
    //System.out.println(PalindromePermutation("Tact Coa".replaceAll(" ", "").toLowerCase()));
    //System.out.println(OneAway("pale", "bake"));
    //System.out.println(StringComparession("aabcccccaa"));
    //RotateMatrix();
    ZeroMatrix();
  }

  // implement an algorithm to determine if a string has all unique characters
  // what is you can not use additional data structure

  public static boolean IsUnique(String sent) {

    boolean[] char_set = new boolean[128];

    for (char i : sent.toCharArray()) {
      if (char_set[i])
        return false;
      char_set[i] = true;
    }

    return true;
    // Map<Character, Boolean> dic = new HashMap<>();

    // for(char i : sent.toCharArray()){
    // if(dic.containsKey(i)) return false;
    // dic.put(i, true);
    // }

    // return true;
  }

  public static boolean CheckPermutation(String one, String two) {

    if (one.length() != two.length())
      return false;

    int[] char_set = new int[128];
    int j = 0;
    for (char i : one.toCharArray()) {
      char_set[i]++;
      char_set[two.charAt(j)]--;
      j++;
    }

    for (int i : char_set) {
      if (i != 0)
        return false;
    }
    // for(char i : two.toCharArray()){
    // char_set[i]--;
    // if(char_set[i] < 0)return false;
    // }

    return true;
  }

  public static String URLify(String url, int len) {

    StringBuilder result = new StringBuilder();
    for (int i = 0; i < len; i++) {

      if (url.charAt(i) == ' ') {
        result.append("%20");
      } else
        result.append(url.charAt(i));
    }

    return result.toString();
  }

  public static boolean PalindromePermutation(String s) {

    int[] set = new int[128];
    for (char c : s.toCharArray()) {
      set[c]++;
    }

    int count = 0;
    for (int i : set) {
      count = count + (i % 2);
    }

    return count <= 1;

  }

  public static boolean OneAway(String a, String b) {
    int[] set = new int [128];
    for(char c: a.toCharArray()){
      set[c]++;
    }

    for(char c : b.toCharArray()){
      set[c]--;
    }
    int count =0;
    for(int i : set){
      if(i != 0) count++;
    }

    return count <2;
  }

  public static String StringComparession(String str){

    String result = "";

    int[] word = new int[128];

    for(char c : str.toCharArray()){
      word[c]++;
    }
    
    for(int i = 0; i< word.length; i++){
      if(word[i] != 0){
        result = result + (char)i + word[i];
      }
    }

    return result;
  }

  private static void printMatrix(int[][]array){
    for(int[] row1 : array){
      for(int col1: row1){
        System.out.printf("%d ", col1);
      }
      System.out.println();
    }
  }

  public static void RotateMatrix(){
    int[][] result =  {{1,2,3},{4,5,6},{7,8,9}};

    for(int k = 0; k< result.length; k++){
      for(int j = k; j< result[k].length; j++){
        int temp = 0;
        temp = result[k][j] ;
        result[k][j] = result[j][k];
        result[j][k] = temp;
      }

    }
  }

  public static void ZeroMatrix(){
    int[][] result =  {{1,2,3},{4,0,6},{7,8,9}};

    int row = -1, col =-1;
    for(int i = 0; i< result.length; i++){
      for(int j = 0; j< result[i].length; j++){
        if(result[i][j] == 0) {
          row = i;
          col = j;
        }
      }
    }
    if(row <= 0 && col <= 0) return;
    for(int i = 0; i<result.length; i++){
      for(int j = 0; j< result[i].length; j++){
        if(row == i || col == j){
          result[i][j] = 0;
        }
      }
    }
    printMatrix(result);
  }

  public static void isSubString(String s1, String s2){
    
  }
}
