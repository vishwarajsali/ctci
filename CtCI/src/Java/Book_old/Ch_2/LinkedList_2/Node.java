package Java.Book_old.Ch_2.LinkedList_2;

public class Node {
    public Node next;
    public int data;
    public Node(int d){
        data = d;
        next = null;
    }

    public void appendToTail(int d){
        Node tail = new Node(d);
        Node n = this;

        while(n.next != null){
            n = n.next;
        }
        n.next = tail;
    }

    public Node deleteNode(Node head, int d){
        Node n = head;

        if(n.data == d) return head.next;

        while(n.next != null){
            if(n.next.data == d){
                n.next = n.next.next;
                return head;
            }
            n = n.next;
        }

        return head;
    }

    public void printSingly(Node n){
        Node a = n;
        while(a != null){
            System.out.printf(" %d -> ",a.data);
            a = a.next;
        }
        System.out.printf("null");

    }

    
}