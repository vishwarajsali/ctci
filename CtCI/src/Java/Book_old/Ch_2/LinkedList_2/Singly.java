package Java.Book_old.Ch_2.LinkedList_2;

/**
 * Singly
 */


public class Singly {

    public static void main(String[] args) {
        Node n = new Node(0);
        n.next = new Node(1);
        n.next.next = new Node(3);

        n.printSingly(n);

        n.deleteNode(n, 1);

        n.printSingly(n);

        n.appendToTail(7);

        n.printSingly(n);
        
    }

   
}



