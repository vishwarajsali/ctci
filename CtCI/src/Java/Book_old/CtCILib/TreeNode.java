package Java.Book_old.CtCILib;

/**
 * TreeNode
 */
public class TreeNode {

    public int data;
    public TreeNode left, right, parent;
    private int size = 0;

    public TreeNode(int data){
        this.data = data;
        size = 1;
    }

    private void setLeftChild(TreeNode left){
        this.left = left;
        if(left != null) left.parent = this;

    }

    private void setRightChild(TreeNode right) {
		this.right = right;
		if (right != null) {
			right.parent = this;
		}
	}

    private static TreeNode createMinimalBST(int[] array, int start, int end){
        if(end < start) return null;

        int mid = (start+end)/2;

        TreeNode n = new TreeNode(array[mid]);
        n.setLeftChild(createMinimalBST(array, start, mid - 1));
		n.setRightChild(createMinimalBST(array, mid + 1, end));
		return n;
    }

    public static TreeNode createMinimalBST(int[] array) {
		return createMinimalBST(array, 0, array.length - 1);
    }
    
    public static TreeNode findNodeByData(TreeNode root, int data){
        if(root == null) return null;
        if(root.data == data) return root;
        else if(root.data< data) return findNodeByData(root.right, data);
        else return findNodeByData(root.left, data) ;
    }


    public static TreeNode findLeftMostNode(TreeNode root){
        if(root == null) return null;
        while(root.left != null){
            root = root.left;
        }

        return root;
    }
}