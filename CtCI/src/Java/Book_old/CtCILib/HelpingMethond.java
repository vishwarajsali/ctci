package Java.Book_old.CtCILib;

import java.util.Random;

/**
 * HelpingMethond
 */
public class HelpingMethond {

    public static int[] createRandomArray(int length, int limt){
        int[] array = new int[length];
        Random random = new Random();
        for(int i = 0; i< length; i++){
            array[i] = random.nextInt(limt);
        }

        return array;
    }

    public static int[] createAsLengthArray(int length){
        int[] array = new int[length];
        
        for(int i = 0; i< length; i++){
            array[i] = i;
        }

        return array;
    }
    
    public static void printArray(int[] array){
        for (int i : array) {
            System.out.printf("%d ", i);
        }
    }

    public static String toFullBinaryString(int a) {
		String s = "";
		for (int i = 0; i < 32; i++) {
			Integer lsb = new Integer(a & 1);
			s = lsb.toString() + s;
			a = a >> 1;
		}
		return s;
	}
}