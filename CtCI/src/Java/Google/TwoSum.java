package Java.Google;
/**
 * TwoSum
 */
public class TwoSum {

    public static void main(String[] args) {
        int[] arr = {1,2,4,4};
        int x = -10;
        boolean an = false;
        for(int i = 0; i< arr.length; i++){
            int diff = Math.abs(arr[i] -x);
            an = search(arr, diff);
            if(an == true) {
                
                System.out.println("Yes"+ diff);
                return;     
            }
        }
        if(an != true) {
            System.out.println("No");     
                  
        } 
        
    }


    public static boolean search(int[] arr, int x){
         
        int start = 0, end = arr.length - 1; 
        while (start <= end) { 
            int m = start + (end - start) / 2; 
            if (arr[m] == x) return true; 
            if (arr[m] < x) start = m + 1; 
            else  end = m - 1; 
        } 
  
        return false;
    }
}