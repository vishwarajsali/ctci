package Java.Video;
import java.util.Stack;

/**
 * MyQueue
 */
public class MyQueue<T> {
    private  Stack<T> stackNewestOnTop = new Stack<>();
    private  Stack<T> stackOldestOnTop = new Stack<>();

    public void enqueue(T value){
        stackNewestOnTop.push(value);
    }

    public T peek(){
        shiftStack();
        return stackOldestOnTop.peek();
    }

    private void shiftStack(){
        if(stackOldestOnTop.isEmpty()){
            while (!stackNewestOnTop.isEmpty()) {
                stackOldestOnTop.push(stackNewestOnTop.pop());
            }
        }
    }

    public T dequeue(){
        shiftStack();
        return stackOldestOnTop.pop();
    }
    
}